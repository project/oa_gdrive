; Open Atrium Google Drive Makefile

api = 2
core = 7.x

libraries[google-api-php-client][download][type] = git
libraries[google-api-php-client][download][url] = https://github.com/google/google-api-php-client.git
libraries[google-api-php-client][download][revision] = 1.1.2
libraries[google-api-php-client][directory_name] = google-api-php-client
libraries[google-api-php-client][destination] = libraries
